#include <stdio.h>
#include<math.h>
int input()
{
	int n;
	printf("enter the value of n");
	scanf("%d",&n);
	return n;
}
float compute(int n)
{
	int i;
	float p,d;
	float sum=0;
	for(i=1;i<=n;i++)
	{
		p=pow(i,2);   
		d=1/p;
		sum=sum+d;
	}
	return sum;
}
void display(float sum)
{
	printf("the sum is %f",sum);
}

int main()
{
	int p;
	float q;
	p=input();
	q=compute(p);
	display(q);
	return 0;
}