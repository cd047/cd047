#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i,j,r,c;
    printf("Enter the number of rows and columns:");
    scanf("%d%d",&r,&c);
    int array1[r][c];
    int array2[r][c];
    int sum[r][c];
    int difference[r][c];
    printf("Provide the entries for array1:\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("Input for element[%d][%d]:\n",i,j);
            scanf("%d",&array1[i][j]);
        }
    }
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d\t",array1[i][j]);
        }
    printf("\n");
    }
    printf("Provide the entries for array2:\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("Input for element[%d][%d]:\n",i,j);
            scanf("%d",&array2[i][j]);
        }
    }
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d\t",array2[i][j]);
        }
    printf("\n");
    }
    printf("\n");
    printf("The Sum of the given two arrays is:\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            sum[i][j]=array1[i][j]+array2[i][j];
            printf("%d\t",sum[i][j]);
        }
    printf("\n");
    }
    printf("\n");
    printf("The difference of the given two arrays is:\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            difference[i][j]=array1[i][j]-array2[i][j];
            printf("%d\t",difference[i][j]);
        }
        printf("\n");
    }
    return 0;
}