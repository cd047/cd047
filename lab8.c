#include <stdio.h>

   struct employee
   {
       int eno;
       char ename[80];
       float salary;
       char DOB[80];
       char companyname[80];
   };
   int main()
   {
       struct employee e1;
       printf("Input for employee number\n");
       scanf("%d",&e1.eno);
       printf("Input the employee name\n");
       scanf("%s",&e1.ename);
       printf("Input for the salary\n");
       scanf("%f",&e1.salary);
       printf("Enter the DOB\n");
       scanf("%s",&e1.DOB);
       printf("Enter the name of the company\n");
       scanf("%s",&e1.companyname);
       printf("\n");
       printf("Employee Details: \n");
       printf("Employee number =%d\n",e1.eno);
       printf("Employee Name = %s\n",e1.ename);
       printf("Employee Salary = %f\n",e1.salary);
       printf("Employee DOB = %s\n",e1.DOB);
       printf("Company name= %s\n",e1.companyname);
       return 0;

   }